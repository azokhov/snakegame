// Fill out your copyright notice in the Description page of Project Settings.


#include "Food_04.h"
#include "PlayerPawnBase.h"

void AFood_04::EatFood(APlayerPawnBase* PlayerPawn, ASnakeBase* Snake)
{
	PlayerPawn->AddLive();
	PlayerPawn->AddScore(FoodValue);
}