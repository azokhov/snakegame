// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PauseMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API UPauseMenuWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UPauseMenuWidget(const FObjectInitializer& ObjectInitializer);

	void NativeConstruct() override;
	
	UPROPERTY(meta = (BindWidget))
		class UButton* ResumeButton;
	UPROPERTY(meta = (BindWidget))
		class UButton* MainMenuButton;

	UFUNCTION()
		void ResumeGame();
	UFUNCTION()
		void GoToMainMenu();
};
