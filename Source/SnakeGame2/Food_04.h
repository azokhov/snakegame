// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "Food_04.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API AFood_04 : public AFood
{
	GENERATED_BODY()

public:

	UFUNCTION()
	void EatFood(APlayerPawnBase* PlayerPawn, ASnakeBase* Snake) override;
	
};
