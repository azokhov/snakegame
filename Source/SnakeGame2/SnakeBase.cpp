// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "PlayerPawnBase.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeGameInstance.h"
#include "NiagaraComponent.h"
#include "Components/AudioComponent.h"
#include "Sound/SoundCue.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StartMovementSpeed = 1.f;
	LastMoveDirection = EMovementDirection::DOWN;
	StartAmountElement = 4;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(StartMovementSpeed);
	USnakeGameInstance* GI = Cast<USnakeGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if(GI->AmountElementComplete > 1)
	{
		AddSnakeElement(GI->AmountElementComplete);
	}
	else
	{
		AddSnakeElement(StartAmountElement);
	}
	MovementSpeed = StartMovementSpeed;	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);	
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	FVector FirstLocation = FVector(900, 0, 0); // FVector::ZeroVector;
	for (int i = 0; i < ElementsNum; ++i)
	{
		for (int j = 0; j < SnakeElements.Num(); j++)
		{
			auto FirstElement = SnakeElements[j];
			FirstLocation = FirstElement->GetActorLocation();
		}
		FTransform NewTransform = FTransform(FirstLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}

	APlayerPawnBase* PawnPTR = Cast<APlayerPawnBase>(GetInstigator());
	PawnPTR->UpdateSnakeLength(SnakeElements.Num());
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	APlayerPawnBase* PawnPTR = Cast<APlayerPawnBase>(GetInstigator());
	float ElementSize = PawnPTR->MapStep;
	
		switch (LastMoveDirection)
		{
		case EMovementDirection::UP:
			MovementVector.X += ElementSize;
			SnakeElements[0]->SetActorRotation(FRotator(0, 0, 0));
			break;
		case EMovementDirection::DOWN:
			MovementVector.X -= ElementSize;
			SnakeElements[0]->SetActorRotation(FRotator(0, 180, 0));
			break;
		case EMovementDirection::RIGHT:
			MovementVector.Y += ElementSize;
			SnakeElements[0]->SetActorRotation(FRotator(0, 90, 0));
			break;
		case EMovementDirection::LEFT:
			MovementVector.Y -= ElementSize;
			SnakeElements[0]->SetActorRotation(FRotator(0, 270, 0));
			break;
		}

		SetChangeDirection(true);

		SnakeElements[0]->NiagaraComponent->Deactivate();
		SnakeElements[0]->AudioComponent->Deactivate();

		FVector FirstLocation = SnakeElements[0]->GetActorLocation();
		FRotator FirstRotation = SnakeElements[0]->GetActorRotation();
		
		SnakeElements[0]->AddActorWorldOffset(MovementVector);

		SnakeElements[0]->NiagaraComponent->Activate();
		SnakeElements[0]->AudioComponent->Activate();
		
		for (int i = 1; i < SnakeElements.Num(); i++)
		{
			SnakeElements[i]->NiagaraComponent->Deactivate();

			FVector SecondLocation = SnakeElements[i]->GetActorLocation();
			SnakeElements[i]->SetActorLocation(FirstLocation);
						
			SnakeElements[i]->NiagaraComponent->Activate();
			FirstLocation = SecondLocation;
		}

}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

/* not working */
//void ASnakeBase::SnakeElementHit(ASnakeElementBase* HitElement, AActor* Other)
//{
//	if (IsValid(HitElement))
//	{
//		int32 ElemIndex;
//		SnakeElements.Find(HitElement, ElemIndex);
//		bool bIsFirst = ElemIndex == 0;
//		IInteractable* HitInterface = Cast<IInteractable>(Other);
//		if (HitInterface)
//		{
//			HitInterface->Hit(this, bIsFirst);
//		}
//	}
//}

bool ASnakeBase::GetChangeDirection()
{
	return bIsChangeDirectionPossible;
}

void ASnakeBase::SetChangeDirection(bool bIsTurned)
{
	bIsChangeDirectionPossible = bIsTurned;
}

void ASnakeBase::SpeedUP(float SpeedBonus)
{
	MovementSpeed = MovementSpeed - SpeedBonus;
	SetActorTickInterval(MovementSpeed);
}

void ASnakeBase::SpeedDown(float SpeedBonus)
{
	MovementSpeed = MovementSpeed + SpeedBonus;
	SetActorTickInterval(MovementSpeed);
}

void ASnakeBase::SnakeHit()
{
	if(!bIsSnakeHit)
	{
		APlayerPawnBase* PawnPTR = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));

		PawnPTR->LossLive();

		UGameplayStatics::PlaySound2D(GetWorld(), HitCue);
	}
	bIsSnakeHit = true;
}

void ASnakeBase::SnakeEat()
{
	UGameplayStatics::PlaySound2D(GetWorld(), EatFoodCue);
	
}

void ASnakeBase::ReSpawnSnake()
{
	SetActorTickEnabled(false);
	GetWorldTimerManager().SetTimer(SnakeSpawnHandle, this, &ASnakeBase::MoveToStart, 2.f, false);
}

void ASnakeBase::MoveToStart()
{
	int32 CurrentAmountElement = SnakeElements.Num();
	APlayerPawnBase* PawnPTR = Cast<APlayerPawnBase>(GetInstigator());
	float ElementSize = PawnPTR->MapStep;
	float Delta = CurrentAmountElement * ElementSize;

	SnakeElements[0]->SetActorLocation(FVector(900 + Delta, 0, 0));
	SetActorTickInterval(StartMovementSpeed);
	MovementSpeed = StartMovementSpeed;
	LastMoveDirection = EMovementDirection::DOWN;
	do
	{
		Move();
		CurrentAmountElement--;
	} while (CurrentAmountElement > 0);
	GetWorldTimerManager().ClearTimer(PawnPTR->SnakeTimerHandle);
	GetWorldTimerManager().SetTimer(
		PawnPTR->SnakeTimerHandle,
		PawnPTR,
		&APlayerPawnBase::LossLive,
		PawnPTR->TimeToDeath,
		false);
	SetActorTickEnabled(true);
	bIsSnakeHit = false;
}
