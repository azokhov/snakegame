// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ALevelActorBase;
class ASnakeBase;
class USoundCue;


UCLASS()
class SNAKEGAME2_API APlayerPawnBase : public APawn
{	
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();
		
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		int32 Lives;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		int32 DefaultsLives;
	UPROPERTY(BlueprintReadWrite)
		int32 Score;
		
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float TimeToDeath;
		FTimerHandle SnakeTimerHandle;

	UPROPERTY(BlueprintReadWrite)
		int32 Keys;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		int32 KeysAmountForWin;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MapStep;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector2D MapSize;
	
	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;

	//How height will be camera
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float CameraHeightCoeff;

	UPROPERTY(BlueprintReadWrite)
		ALevelActorBase* LevelActor;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ALevelActorBase> LevelActorClass;

	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase> SnakeActorClass;

	class ASnakeHUD* SnakeHUD;

		FTimerHandle EnergyHundle;
	UPROPERTY(EditDefaultsOnly)
		float FrequencyUpdate = 0.1f;

	bool IsLevelComplete = false;

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
		USoundCue* LevelCompleteCue;
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
		USoundCue* LossCue;
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
		USoundCue* GameOverCue;
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
		USoundCue* GameWinCue;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void SelectLevel();
	void SetGameLevel();
	void CreateLevelActor();
	void CreateSnakeActor();
		
	void LossLive();
	void AddLive();
	void AddScore(int32 Value);
	void AddTime(float Value);
	void UpdateSnakeTime();
	void UpdateSnakeLength(int32 Length);
	void AddKey();

	void LevelComplete();
	int32 GetLevelNumber();
	void CheckBestScore();

	void WinScreen();

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);
	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);
	UFUNCTION()
	void HandlePauseMenu();
	UFUNCTION()
	void LoadNextLevel();

};
