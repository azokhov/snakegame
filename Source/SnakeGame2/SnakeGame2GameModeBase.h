// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGame2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API ASnakeGame2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
