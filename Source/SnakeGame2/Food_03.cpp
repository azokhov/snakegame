// Fill out your copyright notice in the Description page of Project Settings.


#include "Food_03.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"


void AFood_03::EatFood(APlayerPawnBase* PlayerPawn, ASnakeBase* Snake)
{
	Snake->SpeedUP(SpeedBonus);
	PlayerPawn->AddTime(AddToTimeValue);
	PlayerPawn->AddScore(FoodValue);
}
