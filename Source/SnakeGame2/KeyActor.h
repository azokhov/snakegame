// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "KeyActor.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API AKeyActor : public AFood
{
	GENERATED_BODY()
public:

	UFUNCTION()
	void EatFood(APlayerPawnBase* PlayerPawn, ASnakeBase* Snake) override;
};
