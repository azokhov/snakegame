// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VisualStudioTools/Private/VisualStudioToolsCommandlet.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVisualStudioToolsCommandlet() {}
// Cross Module References
	VISUALSTUDIOTOOLS_API UClass* Z_Construct_UClass_UVisualStudioToolsCommandlet_NoRegister();
	VISUALSTUDIOTOOLS_API UClass* Z_Construct_UClass_UVisualStudioToolsCommandlet();
	ENGINE_API UClass* Z_Construct_UClass_UCommandlet();
	UPackage* Z_Construct_UPackage__Script_VisualStudioTools();
// End Cross Module References
	void UVisualStudioToolsCommandlet::StaticRegisterNativesUVisualStudioToolsCommandlet()
	{
	}
	UClass* Z_Construct_UClass_UVisualStudioToolsCommandlet_NoRegister()
	{
		return UVisualStudioToolsCommandlet::StaticClass();
	}
	struct Z_Construct_UClass_UVisualStudioToolsCommandlet_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVisualStudioToolsCommandlet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommandlet,
		(UObject* (*)())Z_Construct_UPackage__Script_VisualStudioTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVisualStudioToolsCommandlet_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "VisualStudioToolsCommandlet.h" },
		{ "ModuleRelativePath", "Private/VisualStudioToolsCommandlet.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVisualStudioToolsCommandlet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVisualStudioToolsCommandlet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVisualStudioToolsCommandlet_Statics::ClassParams = {
		&UVisualStudioToolsCommandlet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVisualStudioToolsCommandlet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVisualStudioToolsCommandlet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVisualStudioToolsCommandlet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVisualStudioToolsCommandlet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVisualStudioToolsCommandlet, 1323108255);
	template<> VISUALSTUDIOTOOLS_API UClass* StaticClass<UVisualStudioToolsCommandlet>()
	{
		return UVisualStudioToolsCommandlet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVisualStudioToolsCommandlet(Z_Construct_UClass_UVisualStudioToolsCommandlet, &UVisualStudioToolsCommandlet::StaticClass, TEXT("/Script/VisualStudioTools"), TEXT("UVisualStudioToolsCommandlet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVisualStudioToolsCommandlet);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
