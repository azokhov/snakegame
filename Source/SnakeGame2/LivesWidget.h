// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LivesWidget.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API ULivesWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	
public:
	ULivesWidget(const FObjectInitializer& ObjectInitializer);

	void NativeConstruct() override;
	
	void UpdateLives(int32 Value);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* LivesLabel;
	
};
