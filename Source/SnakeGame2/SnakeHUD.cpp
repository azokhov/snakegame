// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeHUD.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeGameInstance.h"
#include "SnakeSaveGame.h"
#include "PlayerPawnBase.h"

#include "HUDWidget.h"
#include "LevelNameWidget.h"
#include "ScoresWidget.h"
#include "BestScoreWidget.h"
#include "LivesWidget.h"
#include "EnergyWidget.h"
#include "LenghtWidget.h"
#include "KeysWidget.h"

#include "PauseMenuWidget.h"
#include "GameOverWidget.h"
#include "LevelCompleteWidget.h"
#include "GameWinWidget.h"


ASnakeHUD::ASnakeHUD()
{
}

// Called when the game starts or when spawned
void ASnakeHUD::BeginPlay()
{
	Super::BeginPlay();

}

void ASnakeHUD::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASnakeHUD::DrawHUD()
{
	Super::DrawHUD();
}

void ASnakeHUD::SetHUDWidget(int32 LevelNumber, int32 Score, int32 Lives, int32 Lenhgt, int32 KeyAmount, int32 KeyAmountForWin)
{
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("BeginPlay HUD"));
	//UE_LOG(LogTemp, Warning, TEXT("BeginPlay SankeHUD called"));
	if (HUDWidgetClass)
	{
		HUDWidget = CreateWidget<UHUDWidget>(GetWorld(), HUDWidgetClass);
		if (HUDWidget)
		{
			GetWorld()->GetFirstPlayerController()->SetInputMode(FInputModeGameOnly());
			HUDWidget->AddToViewport();

			ShowLevelNumber(LevelNumber);
			UpdateScore(Score);
			UpdateLives(Lives);
			UpdateLenght(Lenhgt);
			UpdateKeysAmount(KeyAmount);
			ShowKeysAmountForWin(KeyAmountForWin);
		}
	}
}

void ASnakeHUD::ShowLevelNumber(int32 Value)
{
	if (HUDWidget->LevelNameWidget != nullptr)// crashed here
	{
		HUDWidget->LevelNameWidget->ShowLevelNumber(Value);
	}
}
void ASnakeHUD::UpdateScore(int32 Value)
{
	if (HUDWidget->ScoresWidget)
	{
		HUDWidget->ScoresWidget->UpdateScore(Value);
	}
}
void ASnakeHUD::UpdateLives(int32 Value)
{
	if (HUDWidget->LivesWidget)
	{
		HUDWidget->LivesWidget->UpdateLives(Value);
	}
}
void ASnakeHUD::UpdateEnergy(float Value)
{
	if (HUDWidget->EnergyWidget)
	{
		HUDWidget->EnergyWidget->UpdateEnergy(Value);
	}
}
void ASnakeHUD::UpdateLenght(int32 Lenght)
{
	if (HUDWidget->LenghtWidget)
	{
		HUDWidget->LenghtWidget->UpdateLenght(Lenght);
	}
}
void ASnakeHUD::UpdateKeysAmount(int32 Value)
{
	if (HUDWidget->KeysWidget)
	{
		HUDWidget->KeysWidget->UpdateKeysAmount(Value);
	}
}
void ASnakeHUD::ShowKeysAmountForWin(int32 Value)
{
	if (HUDWidget->KeysWidget)
	{
		HUDWidget->KeysWidget->SetKeysAmountForWin(Value);
	}
}

void ASnakeHUD::LevelComplete(int32 ScoreComplete, int32 LevelNumber)
{
	if (LevelCompleteWidgetClass)
	{
		LevelCompleteWidget = CreateWidget<ULevelCompleteWidget>(GetWorld(), LevelCompleteWidgetClass);
		if (LevelCompleteWidget)
		{
			GetWorld()->GetFirstPlayerController()->bShowMouseCursor = true;
			GetWorld()->GetFirstPlayerController()->SetInputMode(FInputModeGameAndUI());

			HUDWidget->RemoveFromParent();
			LevelCompleteWidget->AddToViewport();
			LevelCompleteWidget->ScoresWidget->UpdateScore(ScoreComplete);
			LevelCompleteWidget->LevelNameWidget->ShowLevelNumber(LevelNumber);
		}
	}
}
void ASnakeHUD::GameWin()
{
	if (GameWinWidgetClass)
	{
		GameWinWidget = CreateWidget<UGameWinWidget>(GetWorld(), GameWinWidgetClass);
		if (GameWinWidget)
		{
			GetWorld()->GetFirstPlayerController()->bShowMouseCursor = true;
			//GetWorld()->GetFirstPlayerController()->SetInputMode(FInputModeGameAndUI());

			FInputModeUIOnly GameWinMenuInputModeData;
			GameWinMenuInputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
			GameWinMenuInputModeData.SetWidgetToFocus(GameWinWidget->TakeWidget());
			GetWorld()->GetFirstPlayerController()->SetInputMode(GameWinMenuInputModeData);

			if(HUDWidget)
			{
				HUDWidget->RemoveFromParent();
			}

			USnakeGameInstance* GI =
				Cast<USnakeGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
			USnakeSaveGame* SG =
				Cast<USnakeSaveGame>(UGameplayStatics::LoadGameFromSlot(TEXT("BestScore"), 0));

			GameWinWidget->ScoresWidget->UpdateScore(GI->ScoreComplete);
			GameWinWidget->BestScoreWidget->UpdateBestScore(SG->BestScore);

			GameWinWidget->AddToViewport();
		}
	}
}
void ASnakeHUD::GameOver()
{
	if (GameOverWidgetClass)
	{
		GameOverWidget = CreateWidget<UGameOverWidget>(GetWorld(), GameOverWidgetClass);
		if (GameOverWidget)
		{
			//GetWorld()->GetFirstPlayerController()->SetPause(true);
			GetWorld()->GetFirstPlayerController()->bShowMouseCursor = true;
			//GetWorld()->GetFirstPlayerController()->SetInputMode(FInputModeUIOnly());
			FInputModeUIOnly GameOverMenuInputModeData;
			GameOverMenuInputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
			GameOverMenuInputModeData.SetWidgetToFocus(GameOverWidget->TakeWidget());
			GetWorld()->GetFirstPlayerController()->SetInputMode(GameOverMenuInputModeData);

			HUDWidget->RemoveFromParent();

			USnakeGameInstance* GI =
				Cast<USnakeGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
			USnakeSaveGame* SG =
				Cast<USnakeSaveGame>(UGameplayStatics::LoadGameFromSlot(TEXT("BestScore"), 0));

			GameOverWidget->ScoresWidget->UpdateScore(GI->ScoreComplete);
			GameOverWidget->BestScoreWidget->UpdateBestScore(SG->BestScore);

			GameOverWidget->AddToViewport();


		}
	}
}

void ASnakeHUD::OpenMainMenu()
{
	UGameplayStatics::OpenLevel(
		GetWorld(),
		FName("MainMenu"),
		true);
}
void ASnakeHUD::PauseGame()
{
	if (PauseMenuWidgetClass)
	{
		PauseMenuWidget = CreateWidget<UPauseMenuWidget>(GetWorld(), PauseMenuWidgetClass);
		if (PauseMenuWidget)
		{
			
			PauseMenuWidget->AddToViewport();
		}
	}	
}
void ASnakeHUD::ResumeGame()
{
	if (PauseMenuWidget)
	{
		PauseMenuWidget->RemoveFromParent();
	}
}
void ASnakeHUD::RestartLevel()
{
	UGameplayStatics::OpenLevel(
		GetWorld(),
		FName(UGameplayStatics::GetCurrentLevelName(GetWorld(), true)),
		true);
	USnakeGameInstance* GI =
		Cast<USnakeGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	APlayerPawnBase* PawnPTR = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if(GI)
	{
		GI->ScoreComplete = 0;
		GI->LivesComplete = PawnPTR->DefaultsLives;
	}	
}
void ASnakeHUD::NewGame()
{
	UGameplayStatics::OpenLevel(GetWorld(), FName("Level_1"));
}
void ASnakeHUD::ExitGame()
{
	UKismetSystemLibrary::QuitGame(
		GetWorld(),
		GetWorld()->GetFirstPlayerController(),
		EQuitPreference::Quit,
		true);
}
