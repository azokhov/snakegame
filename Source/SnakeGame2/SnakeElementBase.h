// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeElementBase.generated.h"

class UStaticMeshComponent;
class UNiagaraComponent;
class UAudioComponent;
class ASnakeBase;

UCLASS()
class SNAKEGAME2_API ASnakeElementBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElementBase();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UNiagaraComponent* NiagaraComponent;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UAudioComponent* AudioComponent;

	UPROPERTY()
		ASnakeBase* SnakeOwner;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
	void SetFirstElementType();
	void SetFirstElementType_Implementation();
		
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	/* not working */
	//virtual void Hit(AActor* Hited, bool bIsHead);

	UFUNCTION()
	void HandleBeginOverlap(
		UPrimitiveComponent* OverlapedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);

	/* not working */
	//UFUNCTION()
	//void HandleHit(
	//	UPrimitiveComponent* HitComponent,
	//	AActor* OtherActor,
	//	UPrimitiveComponent* OtherComp,
	//	FVector NormalImpulse,
	//	const FHitResult& HitResult);
	
};

