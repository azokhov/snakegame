// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "Food_03.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API AFood_03 : public AFood
{
	GENERATED_BODY()

public:

	//variables
	UPROPERTY(EditDefaultsOnly)
	float SpeedBonus;

	//functiion
	UFUNCTION()
	virtual void EatFood(APlayerPawnBase* PlayerPawn, ASnakeBase* Snake) override;
};
