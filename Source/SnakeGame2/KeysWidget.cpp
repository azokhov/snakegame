// Fill out your copyright notice in the Description page of Project Settings.


#include "KeysWidget.h"
#include "Components/TextBlock.h"

UKeysWidget::UKeysWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UKeysWidget::NativeConstruct()
{
	Super::NativeConstruct();
	
}

void UKeysWidget::UpdateKeysAmount(int32 Value)
{
	KeysLabel->SetText(FText::AsNumber(Value));
}

void UKeysWidget::SetKeysAmountForWin(int32 Value)
{
	KeysForWinLabel->SetText(FText::AsNumber(Value));
}
