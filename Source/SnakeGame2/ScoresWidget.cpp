// Fill out your copyright notice in the Description page of Project Settings.


#include "ScoresWidget.h"
#include "Components/TextBlock.h"


UScoresWidget::UScoresWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	
}

void UScoresWidget::NativeConstruct()
{
	Super::NativeConstruct();

}

void UScoresWidget::UpdateScore(int32 Value)
{
	if(ScoreLabel)
	{
		ScoreLabel->SetText(FText::AsNumber(Value));
		
	}
}
