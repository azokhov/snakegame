// Fill out your copyright notice in the Description page of Project Settings.


#include "FloorBase.h"
#include "Floor.h"
#include "Environment.h"
#include "PlayerPawnBase.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"


// Sets default values
AFloorBase::AFloorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	
}

// Called when the game starts or when spawned
void AFloorBase::BeginPlay()
{
	Super::BeginPlay();	
	CreateFloorActor();	
	CreateEnvironment(16);
}

// Called every frame
void AFloorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFloorBase::CreateFloorActor()
{
	FloorActor = GetWorld()->SpawnActor<AFloor>(
		FloorActorClass, 
		FTransform(
			FRotator(FRotator::ZeroRotator), 
			FVector(0, 0, -50), 
			FVector(25,25,1)));
	GetWorld()->SpawnActor<AFloor>(
		FloorActorClass,
		FTransform(
			FRotator(FRotator::ZeroRotator),
			FVector(0, 2500, -50),
			FVector(25, 25, 1)));
	GetWorld()->SpawnActor<AFloor>(
		FloorActorClass,
		FTransform(
			FRotator(FRotator::ZeroRotator),
			FVector(0, -2500, -50),
			FVector(25, 25, 1)));
}

void AFloorBase::CreateEnvironment(int32 Amount)
{
	for(auto i = 0; i <= Amount; i++)
	{
		EnvironmentActor = GetWorld()->SpawnActor<AEnvironment>(
			EnvironmentActorClass,
			FTransform(
				FRotator(0, FMath::RandRange(0, 90), 0),
				FVector(GetRandomLocation().X, GetRandomLocation().Y, -50),
				FVector::OneVector));
	}
}

FVector AFloorBase::GetRandomLocation()
{
	APlayerPawnBase* PawnPTR = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));

	float minX = -PawnPTR->MapSize.X / 2;
	float maxX = PawnPTR->MapSize.X / 2;
	float minY = -PawnPTR->MapSize.Y / 2;
	float maxY = PawnPTR->MapSize.Y / 2;

	int32 Step = PawnPTR->MapStep;

	FVector RandomLocation;
	//TRUE if any blocking or overlapping results are found
	bool bIsBlocking = false;
	int32 Count = 0;
	do
	{
		int32 RandX = FMath::RandRange(minX, maxX);
		int32 RandY = 0;
		if(FMath::RandBool())
		{
			RandY = FMath::RandRange(minY - 8, minY - 3);
		}
		else
		{
			RandY = FMath::RandRange(maxY + 3, maxY + 8);
		}
		int32 X = RandX * Step;
		int32 Y = RandY * Step;

		
		RandomLocation = FVector(X, Y, 0);
		const FQuat Rot;
		const FCollisionShape CollisionShape = FCollisionShape::MakeSphere(49.f);

		bIsBlocking = GetWorld()->OverlapAnyTestByChannel(RandomLocation, Rot, ECC_Visibility, CollisionShape);
		if (bIsBlocking)
		{
			DrawDebugSphere(GetWorld(), RandomLocation, 100.f, 24, FColor::Yellow, false, 0.5f);
		}
		Count++;
	} while (bIsBlocking && Count < 10);


	return RandomLocation;	
}
