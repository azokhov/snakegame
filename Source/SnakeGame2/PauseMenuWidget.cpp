// Fill out your copyright notice in the Description page of Project Settings.


#include "PauseMenuWidget.h"
#include "Components/Button.h"
#include "PlayerPawnBase.h"
#include "Kismet/GameplayStatics.h"

UPauseMenuWidget::UPauseMenuWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UPauseMenuWidget::NativeConstruct()
{
	Super::NativeConstruct();

	ResumeButton->OnClicked.AddUniqueDynamic(this, &UPauseMenuWidget::ResumeGame);
	MainMenuButton->OnClicked.AddUniqueDynamic(this, & UPauseMenuWidget::GoToMainMenu);
	bIsFocusable = true;

}

void UPauseMenuWidget::ResumeGame()
{
	APlayerPawnBase* PawnPTR = Cast <APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(),0));
	PawnPTR->HandlePauseMenu();	
}

void UPauseMenuWidget::GoToMainMenu()
{
	UGameplayStatics::OpenLevel(
		GetWorld(),
		FName("MainMenu"),
		true);
}
