// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "NiagaraComponent.h"
#include "Components/AudioComponent.h"
#include "SnakeBase.h"

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Meshcomponent"));
	RootComponent = MeshComponent;
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

	NiagaraComponent = CreateDefaultSubobject<UNiagaraComponent>(TEXT("Niagaracomponent"));
	//NiagaraComponent->bAutoManageAttachment = true;
	//NiagaraComponent->AutoAttachParent;
	NiagaraComponent->bAutoActivate = false;
	//NiagaraComponent->AutoAttachLocationRule;
	NiagaraComponent->AttachTo(MeshComponent);
	

	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	//AudioComponent->bAutoManageAttachment = true;
	//AudioComponent->AutoAttachParent;
	AudioComponent->bAutoActivate = false;
	//AudioComponent->AutoAttachLocationRule;
	AudioComponent->AttachTo(NiagaraComponent);
	
	

	

	/*  //////////////////  */

	/* not working */

	//MeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Block);
	//MeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	//MeshComponent->SetCollisionObjectType(ECC_Visibility);
}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();

	NiagaraComponent->SetWorldLocation(MeshComponent->GetComponentLocation());
	NiagaraComponent->SetWorldRotation(MeshComponent->GetComponentRotation());
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASnakeElementBase::SetFirstElementType_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);

	/* not working */
	//MeshComponent->OnComponentHit.AddDynamic(this, &ASnakeElementBase::HandleHit);
}

void ASnakeElementBase::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	
	if (IsValid(Snake))// GameOver
	{
		Snake->SnakeHit();
	}
}

/* not working */
//void ASnakeElementBase::Hit(AActor* Hited, bool bIsHead)
//{
//	auto Snake = Cast<ASnakeBase>(Hited);
//
//	if (IsValid(Snake))// GameOver
//	{
//		Snake->SnakeHit();
//	}
//}

void ASnakeElementBase::HandleBeginOverlap(
	UPrimitiveComponent* OverlapedComponent, 
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, 
	int32 OtherBodyIndex, 
	bool bFromSweep, 
	const FHitResult& SweepResult)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
		
	}
	UE_LOG(LogTemp, Warning, TEXT("HandleBeginOverlap"));
}

/* not working */
//void ASnakeElementBase::HandleHit(
//	UPrimitiveComponent* HitComponent, 
//	AActor* OtherActor, 
//	UPrimitiveComponent* OtherComp,
//	FVector NormalImpulse,
//	const FHitResult& HitResult)
//{
//	if (IsValid(SnakeOwner))
//	{
//		SnakeOwner->SnakeElementHit(this, OtherActor);
//		
//	}
//	UE_LOG(LogTemp, Warning, TEXT("HandleHit"));
//}
