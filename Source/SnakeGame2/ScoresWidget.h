// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ScoresWidget.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API UScoresWidget : public UUserWidget
{
	GENERATED_BODY()

protected:

public:
	UScoresWidget(const FObjectInitializer& ObjectInitializer);

	void NativeConstruct() override;

	void UpdateScore(int32 Value);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* ScoreLabel;

};
