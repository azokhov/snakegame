// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "SnakeSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API USnakeSaveGame : public USaveGame
{
	GENERATED_BODY()

public:

	UPROPERTY()
		int32 BestScore = 0;
		
};
