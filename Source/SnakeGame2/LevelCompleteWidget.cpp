// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelCompleteWidget.h"
#include "LevelNameWidget.h"
#include "ScoresWidget.h"
#include "Components/Button.h"
#include "PlayerPawnBase.h"
#include "Kismet/GameplayStatics.h"

ULevelCompleteWidget::ULevelCompleteWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void ULevelCompleteWidget::NativeConstruct()
{
	Super::NativeConstruct();

	bIsFocusable = true;

	ScoresWidget = Cast<UScoresWidget>(GetWidgetFromName(TEXT("WBP_Scores")));
	LevelNameWidget = Cast<ULevelNameWidget>(GetWidgetFromName(TEXT("WBP_LevelName")));
	
	NextLevelButton->OnClicked.AddDynamic(this, &ULevelCompleteWidget::NextLevelButtonCliked);

}

void ULevelCompleteWidget::NextLevelButtonCliked()
{
	APlayerPawnBase* PawnPTR = Cast <APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	PawnPTR->LoadNextLevel();
}
