// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameOverWidget.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API UGameOverWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UGameOverWidget(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;

	UFUNCTION()
	void OnRestartButtonClicked();
	UFUNCTION()
	void OnMainMenuButtonClicked();
	UFUNCTION()
	void OnExitButtonClicked();

	UFUNCTION()
	void ShowScores(int32 Scores);
	UFUNCTION()
	void ShowBestScore(int32 BestScore);

	UPROPERTY(meta = (BindWidget))
		class UButton* RestartButton;
	UPROPERTY(meta = (BindWidget))
		class UButton* MainMenuButton;
	UPROPERTY(meta = (BindWidget))
		class UButton* ExitButton;

	UPROPERTY()
		class UScoresWidget* ScoresWidget;
	UPROPERTY()
		class UBestScoreWidget* BestScoreWidget;
	UPROPERTY()
		class ASnakeHUD* SnakeHUD;
};
