// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BestScoreWidget.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API UBestScoreWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UBestScoreWidget(const FObjectInitializer& ObjectInitializer);

	void NativeConstruct() override;

	void UpdateBestScore(int32 Value);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* BestScoreLabel;
	
};
