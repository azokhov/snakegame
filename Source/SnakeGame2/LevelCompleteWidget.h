// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LevelCompleteWidget.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API ULevelCompleteWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	ULevelCompleteWidget(const FObjectInitializer& ObjectInitializer);

	void NativeConstruct() override;

	UPROPERTY()
		class UScoresWidget* ScoresWidget;
	UPROPERTY()
		class ULevelNameWidget* LevelNameWidget;
	

	UPROPERTY(meta = (BindWidget))
		class UButton* NextLevelButton;

	UFUNCTION()
		void NextLevelButtonCliked();
	
};
