// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LevelActorBase.generated.h"

class AFloorBase;
class AFoodBase;
class AWallBase;

UCLASS()
class SNAKEGAME2_API ALevelActorBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALevelActorBase();
			
	/* Floor Actors */
	UPROPERTY(BlueprintReadWrite)
		AFloorBase* FloorBase;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFloorBase> FloorBaseClass;
	/* Food Actors */
	UPROPERTY(BlueprintReadWrite)
		AFoodBase* FoodBase;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFoodBase> FoodBaseClass;
	/* Wall Actors */
	UPROPERTY(BlueprintReadWrite)
		AWallBase* WallBase;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AWallBase> WallBaseClass;

	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
		
	void CreateFloorBase();
	void CreateFoodBase();
	void CreateWallBase();

	void CreateExitTeleport();
	void PauseSpawnFood();
	
};
