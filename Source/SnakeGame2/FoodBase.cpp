// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodBase.h"
#include "Food.h"
#include "KeyActor.h"
#include "Food_01.h"
#include "Food_02.h"
#include "Food_03.h"
#include "Food_04.h"
#include "DrawDebugHelpers.h"
#include "PlayerPawnBase.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
AFoodBase::AFoodBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	
	SpawnFoodInterval = 5.f;
	TimeToFirstSpawn = 1.f;
}

// Called when the game starts or when spawned
void AFoodBase::BeginPlay()
{
	Super::BeginPlay();
	SetSpawnFoodLocation();
	SetSpawnFoodTimer();	
}

// Called every frame
void AFoodBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFoodBase::CreateFoodActor()
{
	FActorSpawnParameters FoodSpawnParam;
	FoodSpawnParam.Owner = this;
	GetWorld()->SpawnActor<AFood>(FoodActorClass, GetFoodTransform(), FoodSpawnParam);
	
}
void AFoodBase::CreateKeyActor()
{
	FActorSpawnParameters FoodSpawnParam;
	FoodSpawnParam.Owner = this;
	GetWorld()->SpawnActor<AKeyActor>(KeyActorClass, GetFoodTransform(), FoodSpawnParam);
}
void AFoodBase::CreateFood_01()
{	
	FActorSpawnParameters FoodSpawnParam;
	FoodSpawnParam.Owner = this;
	GetWorld()->SpawnActor<AFood_01>(Food_01_Class, GetFoodTransform(), FoodSpawnParam);
	
}
void AFoodBase::CreateFood_02()
{	
	FActorSpawnParameters FoodSpawnParam;
	FoodSpawnParam.Owner = this;
	GetWorld()->SpawnActor<AFood_02>(Food_02_Class, GetFoodTransform(), FoodSpawnParam);
	
}
void AFoodBase::CreateFood_03()
{	
	FActorSpawnParameters FoodSpawnParam;
	FoodSpawnParam.Owner = this;
	GetWorld()->SpawnActor<AFood_03>(Food_03_Class, GetFoodTransform(), FoodSpawnParam);
	
}
void AFoodBase::CreateFood_04()
{
	FActorSpawnParameters FoodSpawnParam;
	FoodSpawnParam.Owner = this;
	GetWorld()->SpawnActor<AFood_04>(Food_04_Class, GetFoodTransform(), FoodSpawnParam);

}

void AFoodBase::SpawnRandomFood()
{
	switch (FMath::RandRange(0, 6))
	{
	case 0:CreateFood_01();
		break;
	case 1:CreateFood_02();
		break;
	case 2:CreateFood_03();
		break;
	case 3:CreateFood_01();
		break;
	case 4:CreateFood_03();
		break;
	case 5:CreateFood_04();
		break;
	case 6:CreateKeyActor();
		break;
	default:CreateFood_01();
		break;
	}
}
void AFoodBase::ReSpawnFood(AFood* Food)
{
	Food->Destroy();
	GetWorldTimerManager().SetTimer(
		ReSpawnFoodHandle, 
		this, 
		&AFoodBase::SpawnRandomFood, 
		SpawnFoodInterval, 
		false, 
		TimeToFirstSpawn);
}

void AFoodBase::SetSpawnFoodTimer()
{
	GetWorldTimerManager().SetTimer(
		SpawnFoodHandle,
		this,
		&AFoodBase::SpawnRandomFood,
		SpawnFoodInterval,
		true,
		TimeToFirstSpawn);
}
void AFoodBase::PauseSpawnFood()
{
	GetWorldTimerManager().PauseTimer(SpawnFoodHandle);	
	GetWorldTimerManager().PauseTimer(ReSpawnFoodHandle);
}
void AFoodBase::SetSpawnFoodLocation()
{
	APlayerPawnBase* PawnPTR = Cast<APlayerPawnBase>(GetInstigator());
	
	float FoodStep = PawnPTR->MapStep;
	FVector2D FoodMapSize = {PawnPTR->MapSize.X, PawnPTR->MapSize.Y};
	int32 FoodX = static_cast<int32>(FoodMapSize.X) % 2;
	int32 FoodY = static_cast<int32>(FoodMapSize.Y) % 2;

	int32 FoodMapMinX = -FoodMapSize.X / 2;
	int32 FoodMapMaxX = FoodMapSize.X / 2 + FoodX;
	int32 FoodMapMinY = -FoodMapSize.Y / 2;
	int32 FoodMapMaxY = FoodMapSize.Y / 2 + FoodY;
	
	for (auto i = FoodMapMinX; i < FoodMapMaxX; i++)
	{
		for (auto j = FoodMapMinY; j < FoodMapMaxY; j++)
		{
			FoodLocations.AddUnique(FVector(i*FoodStep, j*FoodStep, 0));
		}
	}		
}

FTransform AFoodBase::GetFoodTransform()
{
	FTransform SpawnTransform = FTransform(FRotator::ZeroRotator, GetFoodLocation(), FVector(1.5, 1.5, 1.5));
	return SpawnTransform;
}

FVector AFoodBase::GetFoodLocation()
{
	FVector FoodLocation = FoodLocations[1];
	//TRUE if any blocking or overlapping results are found
	bool bIsBlocking = true;
	int32 Count = 0;
	do
	{
		FoodLocation = FoodLocations[FMath::RandRange(0, FoodLocations.Num() - 1)];
		const FQuat Rot;
		const FCollisionShape CollisionShape = FCollisionShape::MakeSphere(49.f);
			
		bIsBlocking = GetWorld()->OverlapAnyTestByChannel(FoodLocation, Rot, ECC_Visibility, CollisionShape);
		if (bIsBlocking)
		{
			DrawDebugSphere(GetWorld(), FoodLocation, 55.f, 24, FColor::Cyan, false, 5.0f);
		}
		Count++;
	} while (bIsBlocking && Count < 10);
				
	return FoodLocation;		
}
