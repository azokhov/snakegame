// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelActorBase.h"
#include "FloorBase.h"
#include "FoodBase.h"
#include "WallBase.h"

// Sets default values
ALevelActorBase::ALevelActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	
}

// Called when the game starts or when spawned
void ALevelActorBase::BeginPlay()
{
	Super::BeginPlay();
			
	CreateFloorBase();
	CreateWallBase();
	CreateFoodBase();
	
}

// Called every frame
void ALevelActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALevelActorBase::CreateFloorBase()
{
	FActorSpawnParameters SpawnParams;
	FloorBase = GetWorld()->SpawnActor<AFloorBase>(FloorBaseClass, SpawnParams);
}

void ALevelActorBase::CreateFoodBase()
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.Instigator = this->GetInstigator();
	FoodBase = GetWorld()->SpawnActor<AFoodBase>(FoodBaseClass, SpawnParams);
}

void ALevelActorBase::CreateWallBase()
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.Instigator = this->GetInstigator();
	WallBase = GetWorld()->SpawnActor<AWallBase>(WallBaseClass, SpawnParams);
}

void ALevelActorBase::CreateExitTeleport()
{
	WallBase->CreateExitTeleport();
}

void ALevelActorBase::PauseSpawnFood()
{
	FoodBase->PauseSpawnFood();
}
