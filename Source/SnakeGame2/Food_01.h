// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "Food_01.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API AFood_01 : public AFood
{
	GENERATED_BODY()

public:
	
	virtual void EatFood(APlayerPawnBase* PlayerPawn, ASnakeBase* Snake) override;
};
