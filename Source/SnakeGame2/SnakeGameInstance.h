// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "SnakeGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API USnakeGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	
	USnakeGameInstance();

	virtual void Init() override;

	UPROPERTY()
		int32 ScoreComplete;
	UPROPERTY()
		int32 LivesComplete;
	UPROPERTY()
		int32 AmountElementComplete = 1;

	
};
