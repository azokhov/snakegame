// Fill out your copyright notice in the Description page of Project Settings.


#include "GameOverWidget.h"
#include "SnakeHUD.h"
#include "ScoresWidget.h"
#include "BestScoreWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"


UGameOverWidget::UGameOverWidget(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
}

void UGameOverWidget::NativeConstruct()
{
	Super::NativeConstruct();

	bIsFocusable = true;
		
	RestartButton->OnClicked.AddDynamic(this, &UGameOverWidget::OnRestartButtonClicked);
	MainMenuButton->OnClicked.AddDynamic(this, &UGameOverWidget::OnMainMenuButtonClicked);
	ExitButton->OnClicked.AddDynamic(this, &UGameOverWidget::OnExitButtonClicked);
	
	ScoresWidget = Cast<UScoresWidget>(GetWidgetFromName(TEXT("WBP_Scores")));
	BestScoreWidget = Cast<UBestScoreWidget>(GetWidgetFromName(TEXT("WBP_BestScore")));
	SnakeHUD = Cast<ASnakeHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
}

void UGameOverWidget::OnRestartButtonClicked()
{
	SnakeHUD->RestartLevel();
}

void UGameOverWidget::OnMainMenuButtonClicked()
{
	SnakeHUD->OpenMainMenu();
}

void UGameOverWidget::OnExitButtonClicked()
{
	SnakeHUD->ExitGame();
}

void UGameOverWidget::ShowScores(int32 Scores)
{
	ScoresWidget->UpdateScore(Scores);
}

void UGameOverWidget::ShowBestScore(int32 BestScore)
{
	BestScoreWidget->UpdateBestScore(BestScore);
}
