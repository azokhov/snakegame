// Fill out your copyright notice in the Description page of Project Settings.


#include "Food_02.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"


void AFood_02::EatFood(APlayerPawnBase* PlayerPawn, ASnakeBase* Snake)
{
	Snake->SpeedDown(SpeedBonus);
	PlayerPawn->AddTime(AddToTimeValue);
	PlayerPawn->AddScore(FoodValue);
}
