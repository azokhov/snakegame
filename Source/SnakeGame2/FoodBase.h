// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FoodBase.generated.h"

class AFood;
class AFood_01;
class AFood_02;
class AFood_03;
class AFood_04;
class AKeyActor;

UCLASS()
class SNAKEGAME2_API AFoodBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFoodBase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn Food Parametres")
		float SpawnFoodInterval;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn Food Parametres")
		float TimeToFirstSpawn;

		FTimerHandle SpawnFoodHandle;
		FTimerHandle ReSpawnFoodHandle;
		TArray<FVector> FoodLocations;
		
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodActorClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood_01> Food_01_Class;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood_02> Food_02_Class;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood_03> Food_03_Class;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood_04> Food_04_Class;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AKeyActor> KeyActorClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	
	void CreateFoodActor();
	void CreateFood_01();
	void CreateFood_02();
	void CreateFood_03();
	void CreateFood_04();
	void CreateKeyActor();

	void SetSpawnFoodLocation();
	void SetSpawnFoodTimer();
	void PauseSpawnFood();
	void ReSpawnFood(AFood* Food);
	void SpawnRandomFood();
					
	FTransform GetFoodTransform();
	FVector GetFoodLocation();
};
