// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LenghtWidget.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API ULenghtWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	ULenghtWidget(const FObjectInitializer& ObjectInitializer);

	void NativeConstruct() override;

	void UpdateLenght(int32 Value);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* LenghtLabel;
};
