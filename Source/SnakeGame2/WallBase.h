// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WallBase.generated.h"

class AWall;
class ATeleportWall;

UCLASS()
class SNAKEGAME2_API AWallBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWallBase();
	
	//Wall actors
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AWall> WallActorClass;
	UPROPERTY()
		TArray<FVector> WallLocations;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float WallHight;
	
	//Teleport actors
	UPROPERTY()
		TArray <ATeleportWall*> TeleportWallActors;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ATeleportWall> TeleportWallActorClass;
	UPROPERTY()
		TArray<FVector> TeleportWallLocations;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float TeleportWallHight;
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetSpawnWallLocations();
	void SetSpawnExtraWallLocations();
	void CreateWallActor();
	void CreateTeleportWallActor();
	FVector GetRandomLocation();
	void CreateExitTeleport();
	
};
