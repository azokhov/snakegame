// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LevelNameWidget.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API ULevelNameWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	ULevelNameWidget(const FObjectInitializer &ObjectInitializer);

	void NativeConstruct() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* LevelNumberLable;

	UFUNCTION()
		void ShowLevelNumber(int32 LevelNumber);
	
};
