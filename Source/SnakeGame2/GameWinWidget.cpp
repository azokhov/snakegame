// Fill out your copyright notice in the Description page of Project Settings.


#include "GameWinWidget.h"
#include "SnakeHUD.h"
#include "ScoresWidget.h"
#include "BestScoreWidget.h"
#include "Components/Button.h"


UGameWinWidget::UGameWinWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UGameWinWidget::NativeConstruct()
{
	Super::NativeConstruct();

	bIsFocusable = true;

	NewGameButton->OnClicked.AddDynamic(this, &UGameWinWidget::OnNewGameButtonClicked);
	MainMenuButton->OnClicked.AddDynamic(this, &UGameWinWidget::OnMainMenuButtonClicked);
	ExitButton->OnClicked.AddDynamic(this, &UGameWinWidget::OnExitButtonClicked);

	ScoresWidget = Cast<UScoresWidget>(GetWidgetFromName(TEXT("WBP_Scores")));
	BestScoreWidget = Cast<UBestScoreWidget>(GetWidgetFromName(TEXT("WBP_BestScore")));
	SnakeHUD = Cast<ASnakeHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
}

void UGameWinWidget::OnNewGameButtonClicked()
{
	SnakeHUD->NewGame();
}

void UGameWinWidget::OnMainMenuButtonClicked()
{
	SnakeHUD->OpenMainMenu();
}

void UGameWinWidget::OnExitButtonClicked()
{
	SnakeHUD->ExitGame();
}

void UGameWinWidget::ShowScores(int32 Score)
{
	ScoresWidget->UpdateScore(Score);
}

void UGameWinWidget::ShowBestScore(int32 BestScore)
{
	BestScoreWidget->UpdateBestScore(BestScore);
}


