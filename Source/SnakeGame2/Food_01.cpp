// Fill out your copyright notice in the Description page of Project Settings.


#include "Food_01.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"


void AFood_01::EatFood(APlayerPawnBase* PlayerPawn, ASnakeBase* Snake)
{
	Snake->AddSnakeElement();
	PlayerPawn->AddTime(AddToTimeValue);
	PlayerPawn->AddScore(FoodValue);
}
