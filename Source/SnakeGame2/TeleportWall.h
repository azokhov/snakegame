// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "TeleportWall.generated.h"

class UStaticMeshComponent;
class UNiagaraComponent;
class UAudioComponent;
class AWallBase;


UCLASS()
class SNAKEGAME2_API ATeleportWall : public AActor, public IInteractable
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATeleportWall();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* TeleportWallComponent;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UNiagaraComponent* NiagaraComponent;
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
		UAudioComponent* AudioComponent;
	
	UPROPERTY()
		AWallBase* WallOwner;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	
};
