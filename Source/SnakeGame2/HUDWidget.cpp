// Fill out your copyright notice in the Description page of Project Settings.


#include "HUDWidget.h"
#include "LevelNameWidget.h"
#include "ScoresWidget.h"
#include "LivesWidget.h"
#include "EnergyWidget.h"
#include "LenghtWidget.h"
#include "KeysWidget.h"


UHUDWidget::UHUDWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UHUDWidget::NativeConstruct()
{
	Super::NativeConstruct();


	LevelNameWidget = Cast<ULevelNameWidget>(GetWidgetFromName(TEXT("WBP_LevelName")));
	ScoresWidget = Cast<UScoresWidget>(GetWidgetFromName(TEXT("WBP_Scores")));
	LivesWidget = Cast<ULivesWidget>(GetWidgetFromName(TEXT("WBP_Lives")));
	EnergyWidget = Cast<UEnergyWidget>(GetWidgetFromName(TEXT("WBP_Energy")));
	LenghtWidget = Cast<ULenghtWidget>(GetWidgetFromName(TEXT("WBP_Lenght")));
	KeysWidget = Cast<UKeysWidget>(GetWidgetFromName(TEXT("WBP_Keys")));
}


