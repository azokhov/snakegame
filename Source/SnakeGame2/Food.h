// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Food.generated.h"

//class AFoodBase;
class UStaticMeshComponent;
class UNiagaraComponent;
class UAudioComponent;
class ASnakeBase;
class APlayerPawnBase;

UCLASS()
class SNAKEGAME2_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Food Parameters")
		int32 FoodValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Food Parameters")
		float FoodLifeSpan;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Food Parameters")
		float AddToTimeValue;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Food Parameters")
		FRotator FoodRotation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Food Parameters")
		float FoodHight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Food Parameters")
		float FloatSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Food Parameters")
		float RotationSpeed;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Property")
		UStaticMeshComponent* FoodMeshComponent;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Property")
		UNiagaraComponent* FoodSpawnEffect;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Property")
		UAudioComponent* SoundEffect;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	virtual void EatFood(APlayerPawnBase* PlayerPawn, ASnakeBase* Snake);
	
			
};
