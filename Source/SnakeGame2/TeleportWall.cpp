// Fill out your copyright notice in the Description page of Project Settings.


#include "TeleportWall.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "NiagaraComponent.h"
#include "Components/AudioComponent.h"
#include "WallBase.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "PlayerPawnBase.h"


// Sets default values
ATeleportWall::ATeleportWall()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	TeleportWallComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TeleportWallComponent"));
	NiagaraComponent = CreateDefaultSubobject<UNiagaraComponent>(TEXT("NiagaraComponent"));
	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	RootComponent = TeleportWallComponent;
	TeleportWallComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	TeleportWallComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

//Called when the game starts or when spawned
void ATeleportWall::BeginPlay()
{
	Super::BeginPlay();

	NiagaraComponent->SetWorldLocation(GetActorLocation());
}

// Called every frame
void ATeleportWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATeleportWall::Interact(AActor* Interactor, bool bIsHead)
{
	if(bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			FVector TeleportTo = FVector::ZeroVector;
			int32 Index;
			APlayerPawnBase* PawnPTR = Cast<APlayerPawnBase>(WallOwner->GetInstigator());
			float MapStep = PawnPTR->MapStep;
			WallOwner->TeleportWallActors.Find(this, Index);

			switch (Index)
			{
			case 0:
				TeleportTo = FVector(WallOwner->TeleportWallActors[3]->GetActorLocation().X - MapStep,
									 WallOwner->TeleportWallActors[3]->GetActorLocation().Y,
									 WallOwner->TeleportWallActors[3]->GetActorLocation().Z);
				Snake->LastMoveDirection = EMovementDirection::DOWN;
				Snake->SnakeElements[0]->SetActorRotation(FRotator(0, 180, 0));
				break;
			case 1:
				TeleportTo = FVector(WallOwner->TeleportWallActors[2]->GetActorLocation().X, 
									 WallOwner->TeleportWallActors[2]->GetActorLocation().Y - MapStep,
									 WallOwner->TeleportWallActors[2]->GetActorLocation().Z);
				Snake->LastMoveDirection = EMovementDirection::LEFT;
				Snake->SnakeElements[0]->SetActorRotation(FRotator(0, 270, 0));
				break;
			case 2:
				TeleportTo = FVector(WallOwner->TeleportWallActors[1]->GetActorLocation().X,
									 WallOwner->TeleportWallActors[1]->GetActorLocation().Y + MapStep,
									 WallOwner->TeleportWallActors[1]->GetActorLocation().Z);
				Snake->LastMoveDirection = EMovementDirection::RIGHT;
				Snake->SnakeElements[0]->SetActorRotation(FRotator(0, 90, 0));
				break;
			case 3:
				TeleportTo = FVector(WallOwner->TeleportWallActors[0]->GetActorLocation().X + MapStep,
									 WallOwner->TeleportWallActors[0]->GetActorLocation().Y, 
									 WallOwner->TeleportWallActors[0]->GetActorLocation().Z);
				Snake->LastMoveDirection = EMovementDirection::UP;
				Snake->SnakeElements[0]->SetActorRotation(FRotator(0, 0, 0));
				break;
			case 4:
				TeleportTo = FVector(WallOwner->TeleportWallActors[3]->GetActorLocation().X + 3*MapStep,
									 WallOwner->TeleportWallActors[3]->GetActorLocation().Y + 3*MapStep,
									 WallOwner->TeleportWallActors[3]->GetActorLocation().Z);
				Snake->LastMoveDirection = EMovementDirection::UP;
				Snake->SnakeElements[0]->SetActorRotation(FRotator(0, 0, 0));
								
				PawnPTR->LevelComplete();
				break;								
			}
			Snake->SnakeElements[0]->SetActorLocation(TeleportTo);
		}
	}
}
