// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"

#include "Components/WidgetComponent.h"

#include "SnakeHUD.generated.h"

class UHUDWidget;
class UPauseMenuWidget;
class UGameOverWidget;
class ULevelCompleteWidget;
class UGameWinWidget;


/**
 * 
 */
UCLASS()
class SNAKEGAME2_API ASnakeHUD : public AHUD
{
	GENERATED_BODY()

public:
	ASnakeHUD();
	
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	virtual void DrawHUD() override;

	UFUNCTION()
		void SetHUDWidget(int32 LevelNumber, int32 Score, int32 Lives, int32 Lenhgt, int32 KeyAmount, int32 KeyAmountForWin);
	UFUNCTION()
		void ShowLevelNumber(int32 Value);
	UFUNCTION()
		void UpdateScore(int32 Value);
	UFUNCTION()
		void UpdateLives(int32 Value);
	UFUNCTION()
		void UpdateEnergy(float Value);
	UFUNCTION()
		void UpdateLenght(int32 Lenght);
	UFUNCTION()
		void UpdateKeysAmount(int32 Value);
	UFUNCTION()
		void ShowKeysAmountForWin(int32 Value);

	UFUNCTION()
		void LevelComplete(int32 ScoreComplete, int32 LevelNumber);
	UFUNCTION()
		void GameWin();
	UFUNCTION()
		void GameOver();

	UFUNCTION()
		void OpenMainMenu();
	UFUNCTION()
		void PauseGame();
	UFUNCTION()
		void ResumeGame();
	UFUNCTION()
		void RestartLevel();
	UFUNCTION()
		void NewGame();
	UFUNCTION()
		void ExitGame();

	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
		TSubclassOf<UUserWidget> HUDWidgetClass;
	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
		TSubclassOf<UUserWidget> PauseMenuWidgetClass;
	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
		TSubclassOf<UUserWidget> GameOverWidgetClass;
	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
		TSubclassOf<UUserWidget> LevelCompleteWidgetClass;
	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
		TSubclassOf<UUserWidget> GameWinWidgetClass;
	
private:

	UHUDWidget* HUDWidget;
	UPauseMenuWidget* PauseMenuWidget;
	UGameOverWidget* GameOverWidget;
	ULevelCompleteWidget* LevelCompleteWidget;
	UGameWinWidget* GameWinWidget;
};
