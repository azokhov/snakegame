// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class USoundCue;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME2_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();
		
	FTimerHandle SnakeSpawnHandle;
	UPROPERTY(EditDefaultsOnly)
		int32 StartAmountElement;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
		float StartMovementSpeed;
		float MovementSpeed;
		

	UPROPERTY()
		bool bIsChangeDirectionPossible = true;
		bool bIsSnakeHit = false;

	UPROPERTY(BlueprintReadWrite)
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
		EMovementDirection LastMoveDirection;

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
		USoundCue* EatFoodCue; 
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
		USoundCue* HitCue;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1);

	UFUNCTION(BlueprintCallable)
	void Move();

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	/* not working */
	//UFUNCTION()
	//void SnakeElementHit(ASnakeElementBase* HitElement, AActor* Other);
	
	bool GetChangeDirection();
	void SetChangeDirection(bool bIsTurned);
	
	
	UFUNCTION(BlueprintCallable)
	void SpeedUP(float SpeedBonus);
	UFUNCTION(BlueprintCallable)
	void SpeedDown(float SpeedBonus);
	UFUNCTION(BlueprintCallable)
	void SnakeHit();
	void SnakeEat();
	UFUNCTION()
	void ReSpawnSnake();
	void MoveToStart();
	
};
