// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "KeysWidget.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API UKeysWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UKeysWidget(const FObjectInitializer& ObjectInitializer);

	void NativeConstruct() override;

	void UpdateKeysAmount(int32 Value);
	void SetKeysAmountForWin(int32 Value);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* KeysLabel;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* KeysForWinLabel;
};
