// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelNameWidget.h"
#include "Components/TextBlock.h"

ULevelNameWidget::ULevelNameWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void ULevelNameWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

void ULevelNameWidget::ShowLevelNumber(int32 LevelNumber)
{
	LevelNumberLable->SetText(FText::AsNumber(LevelNumber));
}
