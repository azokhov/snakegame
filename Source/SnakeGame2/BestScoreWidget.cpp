// Fill out your copyright notice in the Description page of Project Settings.


#include "BestScoreWidget.h"
#include "Components/TextBlock.h"

UBestScoreWidget::UBestScoreWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UBestScoreWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

void UBestScoreWidget::UpdateBestScore(int32 Value)
{
	if (BestScoreLabel)
	{
		BestScoreLabel->SetText(FText::AsNumber(Value));
	}
}
