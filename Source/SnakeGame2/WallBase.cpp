// Fill out your copyright notice in the Description page of Project Settings.


#include "WallBase.h"
#include "Wall.h"
#include "TeleportWall.h"
#include "PlayerPawnBase.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "NiagaraComponent.h"
#include "Components/AudioComponent.h"

// Sets default values
AWallBase::AWallBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	WallHight = -30;
	TeleportWallHight = 20;
}

// Called when the game starts or when spawned
void AWallBase::BeginPlay()
{
	Super::BeginPlay();

	SetSpawnWallLocations();
	SetSpawnExtraWallLocations();
	CreateWallActor();
	CreateTeleportWallActor();
}

// Called every frame
void AWallBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AWallBase::SetSpawnWallLocations()
{
	APlayerPawnBase* PawnPTR = Cast< APlayerPawnBase>(GetInstigator());

	float WallStep = PawnPTR->MapStep;
	
	FVector2D WallMapSize = { PawnPTR->MapSize.X+2, PawnPTR->MapSize.Y+2 };
	int32 WallX = static_cast<int32>(WallMapSize.X) % 2;
	int32 WallY = static_cast<int32>(WallMapSize.Y) % 2;

	int32 WallMinX = -WallMapSize.X / 2;
	int32 WallMaxX = WallMapSize.X / 2 + WallX;
	int32 WallMinY = -WallMapSize.Y / 2;
	int32 WallMaxY = WallMapSize.Y / 2 + WallY;

	for (auto i = WallMinX; i < WallMaxX; i++)
	{
		for (auto j = WallMinY; j < WallMaxY; j++)
		{
			if(i < -PawnPTR->MapSize.X/2 || i > (PawnPTR->MapSize.X/2 - 1 + WallX) ||
			   j < -PawnPTR->MapSize.Y/2 || j > (PawnPTR->MapSize.Y/2 - 1 + WallY))
			{
				if (j == 0 || i == 0)
				{
					TeleportWallLocations.AddUnique(FVector(i * WallStep, j * WallStep, TeleportWallHight));
				}
				else
				{
					WallLocations.AddUnique(FVector(i * WallStep, j * WallStep, WallHight));
				}				
			}
		}
	}		
}

void AWallBase::SetSpawnExtraWallLocations()
{
	APlayerPawnBase* PawnPTR = Cast< APlayerPawnBase>(GetInstigator());
	float WallStep = PawnPTR->MapStep;
	
	switch (PawnPTR->GetLevelNumber())
	{
	case 1:
		break;
	case 2:
		for (auto i = -1; i <= 1; i++)
		{
			for (auto j = -1; j <= 1; j++)
			{
				if (abs(i) != abs(j) || i == 0)
				{
					WallLocations.AddUnique(FVector(i * WallStep, j * WallStep, WallHight));
				}
			}
		}	
		break;
	case 3:
		for (auto i = -4; i <= 4; i++)
		{
			for (auto j = -5; j <= 5; j++)
			{
				if ((3 <= abs(i) && abs(i) <= 4) && (4 <= abs(j) && abs(j) <= 5))
				{
					WallLocations.AddUnique(FVector(i * WallStep, j * WallStep, WallHight));
					if (abs(i) == 4 && abs(j) == 5)
					{
						WallLocations.Remove(FVector(i * WallStep, j * WallStep, WallHight));
					}
				}
			}
		}	
		break;
	case 4:
		for (auto i = -3; i <= 3; i++)
		{
			for (auto j = -4; j <= 4; j++)
			{
				if ((-1 <= i && i <= 1 && abs(j) == 4) || //create vertical lines;
					(-1 <= j && j <= 1 && abs(i) == 3) || //create horizontal lines;
					(i == j && j == 0))					  //add zero point;
				{
					WallLocations.AddUnique(FVector(i * WallStep, j * WallStep, WallHight));
				}
			}
		}	
		break;
	case 5:
		/* Create centre */
		for (auto i = -2; i <= 2; i++)
		{
			for (auto j = -2; j <= 2; j++)
			{
				if ((-2 <= i && i <= 2 && abs(j) == 0) ||
					(-2 <= j && j <= 2 && abs(i) == 0) ||
					(-1 <= i && i <= 1 && -1 <= j && j <= 1))
				{
					WallLocations.AddUnique(FVector(i * WallStep, j * WallStep, WallHight));
				}
			}
		}
		/* Create corners */
		for (auto i = -7; i <= 7; i++)
		{
			for (auto j = -8; j <= 8; j++)
			{
				if (5 <= abs(i) && abs(i) <= 7 && abs(j) == 8 ||
					6 <= abs(j) && abs(j) <= 8 && abs(i) == 7 ||
					abs(i) == 6 && abs(j) == 7)
				{
					WallLocations.AddUnique(FVector(i * WallStep, j * WallStep, WallHight));
				}
			}
		}
		break;
	case 6:
		for (auto i = -4; i <= 4; i++)
		{
			for (auto j = -4; j <= 4; j++)
			{
				if (3 <= abs(i) && abs(i) <= 4 && abs(j) == 4 ||
					3 <= abs(j) && abs(j) <= 4 && abs(i) == 4 ||
					-1 <= i && i <= 1 && j == 0 ||
					-1 <= j && j <= 1 && i == 0)
				{
					WallLocations.AddUnique(FVector(i * WallStep, j * WallStep, WallHight));
				}
			}
		}
		break;
	default:
		break;
	}	
}

void AWallBase::CreateWallActor()
{	
	for(auto i = 0; i < WallLocations.Num(); i++)
	{
		GetWorld()->SpawnActor<AWall>(WallActorClass,
		FTransform(FRotator::ZeroRotator, WallLocations[i], FVector::OneVector));
	}	
}

void AWallBase::CreateTeleportWallActor()
{
	for (auto i = 0; i < TeleportWallLocations.Num(); i++)
	{
		ATeleportWall* NewTeleportWall = GetWorld()->SpawnActor<ATeleportWall>(TeleportWallActorClass,
			FTransform(FRotator::ZeroRotator, TeleportWallLocations[i], FVector(0.9, 0.9,0.9)));
		NewTeleportWall->WallOwner = this;
		TeleportWallActors.Add(NewTeleportWall);		
	}
}

FVector AWallBase::GetRandomLocation()
{
	APlayerPawnBase* PawnPTR = Cast< APlayerPawnBase>(GetInstigator());
	
	float minX = -PawnPTR->MapSize.X / 2;
	float maxX = PawnPTR->MapSize.X / 2;
	float minY = -PawnPTR->MapSize.Y / 2;
	float maxY = PawnPTR->MapSize.Y / 2;

	int32 Step = PawnPTR->MapStep;
		
	FVector RandomLocation;
	//TRUE if any blocking or overlapping results are found
	bool bIsBlocking = false;
	int32 Count = 0;
	do
	{
		int32 RandX = FMath::RandRange(minX, maxX);
		int32 RandY = FMath::RandRange(minY, maxY);
		int32 X = RandX * Step;
		int32 Y = RandY * Step;

		//RandomLocation = FoodLocations[FMath::RandRange(0, WallLocations.Num() - 1)];
		RandomLocation = FVector(X, Y, TeleportWallHight);
		const FQuat Rot;
		const FCollisionShape CollisionShape = FCollisionShape::MakeSphere(49.f);

		bIsBlocking = GetWorld()->OverlapAnyTestByChannel(RandomLocation, Rot, ECC_Visibility, CollisionShape);
		if (bIsBlocking)
		{
			DrawDebugSphere(GetWorld(), RandomLocation, 55.f, 24, FColor::Red, false, 5.0f);
		}
		Count++;
	} while (bIsBlocking && Count < 10);

		
	return RandomLocation;
}

void AWallBase::CreateExitTeleport()
{
	ATeleportWall* NewTeleportWall = GetWorld()->SpawnActor<ATeleportWall>(TeleportWallActorClass,
		FTransform(FRotator::ZeroRotator, GetRandomLocation(), FVector(0.9, 0.9, 0.9)));
	NewTeleportWall->WallOwner = this;
	TeleportWallActors.Add(NewTeleportWall);

	NewTeleportWall->NiagaraComponent->Activate();
	NewTeleportWall->AudioComponent->Play();
}

