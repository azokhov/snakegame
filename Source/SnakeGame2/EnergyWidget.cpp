// Fill out your copyright notice in the Description page of Project Settings.


#include "EnergyWidget.h"
#include "Components/ProgressBar.h"

UEnergyWidget::UEnergyWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UEnergyWidget::NativeConstruct()
{
	Super::NativeConstruct();
		
}

void UEnergyWidget::UpdateEnergy(float Value)
{
	EnergyBar->SetPercent(Value);
}
