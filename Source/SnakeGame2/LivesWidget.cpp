// Fill out your copyright notice in the Description page of Project Settings.


#include "LivesWidget.h"
#include <Components/TextBlock.h>


ULivesWidget::ULivesWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void ULivesWidget::NativeConstruct()
{
	Super::NativeConstruct();

}

void ULivesWidget::UpdateLives(int32 Value)
{
	LivesLabel->SetText(FText::AsNumber(Value));
}
