// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "EnergyWidget.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API UEnergyWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UEnergyWidget(const FObjectInitializer& ObjectInitializer);

	void NativeConstruct() override;

	void UpdateEnergy(float Value);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UProgressBar* EnergyBar;
};
