// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FloorBase.generated.h"

class AFloor;
class AEnvironment;

UCLASS()
class SNAKEGAME2_API AFloorBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFloorBase();

	UPROPERTY(BlueprintReadWrite)
		AFloor* FloorActor;
	UPROPERTY(BlueprintReadWrite)
		AEnvironment* EnvironmentActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFloor> FloorActorClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AEnvironment> EnvironmentActorClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void CreateFloorActor();
	void CreateEnvironment(int32 Amount);
	FVector GetRandomLocation();
		
};
