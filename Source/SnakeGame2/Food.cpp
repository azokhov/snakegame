// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "FoodBase.h"
#include "Components/StaticMeshComponent.h"
#include "NiagaraComponent.h"
#include "Components/AudioComponent.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
		
	FoodMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	FoodSpawnEffect = CreateDefaultSubobject<UNiagaraComponent>(TEXT("NiagaraComponent"));
	SoundEffect = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	
	FoodMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	FoodMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	RootComponent = FoodMeshComponent;
	
	FoodSpawnEffect->AttachTo(FoodMeshComponent);
	SoundEffect->AttachTo(FoodSpawnEffect);

	FoodRotation = FRotator::ZeroRotator;
	FoodHight = 0;
	FloatSpeed = 20.0f;
	RotationSpeed = 20.0f;
	FoodLifeSpan = 5.f;
	FoodValue = 50;
	AddToTimeValue = 6.f;
}

 //Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();	
		
	SetActorRotation(FoodRotation);
	FVector NewLocation = FVector(GetActorLocation().X, GetActorLocation().Y, FoodHight);
	SetActorLocation(NewLocation);
	SetLifeSpan(FoodLifeSpan);
	FoodSpawnEffect->SetWorldRotation(FRotator::ZeroRotator);
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	FVector NewLocation = GetActorLocation();
	FRotator NewRotation = GetActorRotation();
	float RunningTime = GetGameTimeSinceCreation();
	float DeltaHeight = (FMath::Sin(RunningTime + DeltaTime) - FMath::Sin(RunningTime));
	NewLocation.Z += DeltaHeight * FloatSpeed;       //Scale our height by a factor of 20
	float DeltaRotation = DeltaTime * RotationSpeed;    //Rotate by 20 degrees per second
	NewRotation.Yaw += DeltaRotation;
	SetActorLocationAndRotation(NewLocation, NewRotation);	
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{		
	AFoodBase* FoodOwner = Cast<AFoodBase>(GetOwner());
	FoodOwner->ReSpawnFood(this);
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			APlayerPawnBase* PawnPTR = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
			EatFood(PawnPTR, Snake);
			Snake->SnakeEat();			
		}
	}
}

void AFood::EatFood(APlayerPawnBase* PlayerPawn, ASnakeBase* Snake)
{
}