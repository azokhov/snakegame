// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "LevelActorBase.h"
#include "SnakeBase.h"
#include "Food.h"
#include "Engine.h"
#include "Components/Inputcomponent.h"
#include "SnakeHUD.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetStringLibrary.h"
#include "SnakeGameInstance.h"
#include "SnakeSaveGame.h"




// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
	CameraHeightCoeff = 888.75f;
	MapStep = 100;
	MapSize = { 12,12 };
	Lives = 3;
	DefaultsLives = 3;
	Score = 0;
	TimeToDeath = 20;
	Keys = 0;
	KeysAmountForWin = 5;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
		
	SelectLevel();

}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);	
}

void APlayerPawnBase::SelectLevel()
{
	CreateSnakeActor();
	
	USnakeGameInstance* GI = Cast<USnakeGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	
	switch (GetLevelNumber())
	{
	case 1:		
		KeysAmountForWin = 1;
		Lives = DefaultsLives;
		SetGameLevel();
		break;
	case 2:		
		Score = GI->ScoreComplete;
		Lives = GI->LivesComplete;
		KeysAmountForWin = 2;
		SetGameLevel();
		break;
	case 3:		
		Score = GI->ScoreComplete;
		Lives = GI->LivesComplete;
		KeysAmountForWin = 3;
		SetGameLevel();
		break;
	case 4:		
		Score = GI->ScoreComplete;
		Lives = GI->LivesComplete;
		KeysAmountForWin = 4;
		SetGameLevel();
		break;
	case 5:		
		Score = GI->ScoreComplete;
		Lives = GI->LivesComplete;
		KeysAmountForWin = 5;
		SetGameLevel();
		break;
	case 6:		
		Score = GI->ScoreComplete;
		Lives = GI->LivesComplete;
		KeysAmountForWin = 6;
		SetGameLevel();
		break;
	default:		
		SetActorLocationAndRotation(FVector(800, 10, 130), FRotator(-5, 178, 0));
		SnakeHUD = Cast<ASnakeHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
		if (SnakeHUD)
		{
			SnakeHUD->GameWin();
		}
		break;
	}
}

void APlayerPawnBase::SetGameLevel()
{
	FVector2D ScreenResolution;
	GEngine->GameViewport->GetViewportSize(ScreenResolution);
	float CameraHeight;
	CameraHeight = ScreenResolution.X / ScreenResolution.Y * CameraHeightCoeff;
	SetActorLocationAndRotation(FVector(0, 0, CameraHeight), FRotator(-90, 0, 0));
	
	CreateLevelActor();

	GetWorldTimerManager().SetTimer(SnakeTimerHandle, this, &APlayerPawnBase::LossLive, TimeToDeath, false);

	SnakeHUD = Cast<ASnakeHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
	if (SnakeHUD)
	{
		SnakeHUD->SetHUDWidget(
			GetLevelNumber(), 
			Score, 
			Lives, 
			SnakeActor->SnakeElements.Num(), 
			Keys, 
			KeysAmountForWin);		
	}

	GetWorldTimerManager().SetTimer(EnergyHundle, this, &APlayerPawnBase::UpdateSnakeTime, FrequencyUpdate, true);
	
}

void APlayerPawnBase::LossLive()
{
	Lives = Lives--;

	//GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayWorldCameraShake

	if (SnakeHUD)
	{
		SnakeHUD->UpdateLives(Lives);
	}
	if (Lives > 0)
	{
		GetWorldTimerManager().PauseTimer(SnakeTimerHandle);
		SnakeActor->ReSpawnSnake();
		//Play loss music
		UGameplayStatics::PlaySound2D(GetWorld(), LossCue);
	}
	else
	{
		if(IsValid(SnakeActor))
		{
			CheckBestScore();
			SnakeActor->SetActorTickEnabled(false);
			GetWorldTimerManager().PauseTimer(SnakeTimerHandle);
			GetWorldTimerManager().PauseTimer(EnergyHundle);
			SnakeHUD->GameOver();//GameOver Screen
			UGameplayStatics::PlaySound2D(GetWorld(), GameOverCue);
		}
	}
}
void APlayerPawnBase::AddLive()
{
	Lives = Lives++;
	if (SnakeHUD)
	{
		SnakeHUD->UpdateLives(Lives);
	}
}
void APlayerPawnBase::AddScore(int32 Value)
{
	Score += Value;
	if (SnakeHUD)
	{
		SnakeHUD->UpdateScore(Score);
	}
}
void APlayerPawnBase::AddTime(float Value)
{
	float RemTime = GetWorldTimerManager().GetTimerRemaining(SnakeTimerHandle);
	GetWorldTimerManager().ClearTimer(SnakeTimerHandle);
	if (RemTime + Value <= TimeToDeath)
	{
		GetWorldTimerManager().SetTimer(SnakeTimerHandle, this, &APlayerPawnBase::LossLive, RemTime + Value, false);
	}
	else
	{
		GetWorldTimerManager().SetTimer(SnakeTimerHandle, this, &APlayerPawnBase::LossLive, TimeToDeath, false);
	}
}
void APlayerPawnBase::UpdateSnakeTime()
{
	float RemainingTime = GetWorldTimerManager().GetTimerRemaining(SnakeTimerHandle);
	float MaxTime = TimeToDeath;

	if (SnakeHUD)
	{
		SnakeHUD->UpdateEnergy(RemainingTime/MaxTime);
	}
}
void APlayerPawnBase::UpdateSnakeLength(int32 Lenght)
{
	if (SnakeHUD)
	{
		SnakeHUD->UpdateLenght(Lenght);
	}
}
void APlayerPawnBase::AddKey()
{
	Keys = Keys++;
	
	if (Keys == KeysAmountForWin)
	{
		LevelActor->CreateExitTeleport();
		LevelActor->PauseSpawnFood();
		GetWorldTimerManager().PauseTimer(SnakeTimerHandle);
		GetWorldTimerManager().PauseTimer(EnergyHundle);
				
		TArray <AActor*> FoodActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AFood::StaticClass(), FoodActors);
		for (auto Food : FoodActors)
		{
			Food->Destroy();
		}
	}
	if (SnakeHUD)
	{
		SnakeHUD->UpdateKeysAmount(Keys);
	}
}

void APlayerPawnBase::LevelComplete()
{
	FString Level = UGameplayStatics::GetCurrentLevelName(GetWorld(), true);
	if (Level == "Level_6")
	{
		FTimerHandle WinScreenHandle;
		GetWorldTimerManager().SetTimer(WinScreenHandle, this, &APlayerPawnBase::WinScreen, 3.f, false);
		UGameplayStatics::PlaySound2D(GetWorld(), GameWinCue);
	}
	else
	{
		USnakeGameInstance* GI = Cast<USnakeGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
		GI->ScoreComplete = Score;
		GI->LivesComplete = Lives;
		GI->AmountElementComplete = SnakeActor->SnakeElements.Num();

		SnakeHUD->LevelComplete(GI->ScoreComplete, GetLevelNumber());
		
		IsLevelComplete = true;

		UGameplayStatics::PlaySound2D(GetWorld(), LevelCompleteCue);
	}
}

int32 APlayerPawnBase::GetLevelNumber()
{
	FString LevelName = UGameplayStatics::GetCurrentLevelName(GetWorld(), true);
	FString LevelNumberString = "null";
	int32 LevelNumber = 0;
	if (LevelName.Split("_", nullptr, &LevelNumberString))
	{
		//LevelNumber = FCString::Atoi(*LevelNumberString);

		LevelNumber = UKismetStringLibrary::Conv_StringToInt(LevelNumberString);

	}
	return LevelNumber;
}

void APlayerPawnBase::CheckBestScore()
{
	USnakeGameInstance* GI =
		Cast<USnakeGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	GI->ScoreComplete = Score;

	USnakeSaveGame* LoadGame =
		Cast<USnakeSaveGame>(UGameplayStatics::LoadGameFromSlot(TEXT("BestScore"), 0));
	if (LoadGame)
	{
		if (Score > LoadGame->BestScore)
		{
			LoadGame->BestScore = Score;
			UGameplayStatics::SaveGameToSlot(LoadGame, TEXT("BestScore"), 0); // New Record
		}
	}
	else
	{
		USnakeSaveGame* SG =
			Cast<USnakeSaveGame>(UGameplayStatics::CreateSaveGameObject(USnakeSaveGame::StaticClass()));
		SG->BestScore = Score;
		UGameplayStatics::SaveGameToSlot(SG, TEXT("BestScore"), 0); // First Record
	}
}

void APlayerPawnBase::WinScreen()
{
	CheckBestScore();
	UGameplayStatics::OpenLevel(GetWorld(), FName("GameWinLevel"));	
}

void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
	PlayerInputComponent->BindAction("OpenPauseMenu", IE_Pressed, this, &APlayerPawnBase::HandlePauseMenu).bExecuteWhenPaused = true;
	PlayerInputComponent->BindAction("NextLevel", IE_Pressed, this, &APlayerPawnBase::LoadNextLevel);
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor) && !IsLevelComplete)
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN)
		{
			if (SnakeActor->GetChangeDirection())
			{
				SnakeActor->LastMoveDirection = EMovementDirection::UP;
				SnakeActor->SetChangeDirection(false);				
			}
			return;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			if (SnakeActor->GetChangeDirection())
			{
				SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
				SnakeActor->SetChangeDirection(false);				
			}
			return;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor) && !IsLevelComplete)
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			if (SnakeActor->GetChangeDirection())
			{
				SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
				SnakeActor->SetChangeDirection(false);				
			}
			return;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			if (SnakeActor->GetChangeDirection())
			{
				SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
				SnakeActor->SetChangeDirection(false);				
			}
			return;
		}
	}
}

void APlayerPawnBase::HandlePauseMenu()
{
	APlayerController* PC = Cast<APlayerController>(GetController());
	UE_LOG(LogTemp, Warning, TEXT("HandlePauseMenu called"));

	if (!IsLevelComplete)
	{
		PC->SetPause(!PC->IsPaused());

		if (!PC->IsPaused())
		{
			PC->bShowMouseCursor = false;
			PC->SetInputMode(FInputModeGameOnly());
			SnakeHUD->ResumeGame();

			UE_LOG(LogTemp, Warning, TEXT("ResumeGame"));
		}
		else
		{
			
			PC->bShowMouseCursor = true;
			PC->SetInputMode(FInputModeGameAndUI());
			SnakeHUD->PauseGame();

			UE_LOG(LogTemp, Warning, TEXT("PauseGame"));
			
		}
	}
}

void APlayerPawnBase::LoadNextLevel()
{
	if(IsLevelComplete)
	{
		int32 LevelNumber = GetLevelNumber();
		FString NextLevel = "Level_" + FString::FromInt(++LevelNumber);
		UGameplayStatics::OpenLevel(GetWorld(), FName(NextLevel));
	}
}

void APlayerPawnBase::CreateLevelActor()
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.Instigator = this;
	LevelActor = GetWorld()->SpawnActor<ALevelActorBase>(LevelActorClass, SpawnParams);
}

void APlayerPawnBase::CreateSnakeActor()
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.Instigator = this;
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, SpawnParams);
}
