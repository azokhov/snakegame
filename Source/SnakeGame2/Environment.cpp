// Fill out your copyright notice in the Description page of Project Settings.


#include "Environment.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AEnvironment::AEnvironment()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

}

// Called when the game starts or when spawned
void AEnvironment::BeginPlay()
{
	Super::BeginPlay();
	
	MeshComponent->SetStaticMesh(EnvironmentMeshArray[FMath::RandRange(0, EnvironmentMeshArray.Num()-1)]);
}

// Called every frame
void AEnvironment::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

