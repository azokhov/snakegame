// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HUDWidget.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API UHUDWidget : public UUserWidget
{
	GENERATED_BODY()
protected:

public:
	UHUDWidget(const FObjectInitializer& ObjectInitializer);

	void NativeConstruct() override;

	UPROPERTY()
		class ULevelNameWidget* LevelNameWidget;
	UPROPERTY()
		class UScoresWidget* ScoresWidget;
	UPROPERTY()
		class ULivesWidget* LivesWidget;
	UPROPERTY()
		class UEnergyWidget* EnergyWidget;
	UPROPERTY()
		class ULenghtWidget* LenghtWidget;
	UPROPERTY()
		class UKeysWidget* KeysWidget;
	
};
