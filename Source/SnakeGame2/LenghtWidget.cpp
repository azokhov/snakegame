// Fill out your copyright notice in the Description page of Project Settings.


#include "LenghtWidget.h"
#include "Components/TextBlock.h"

ULenghtWidget::ULenghtWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void ULenghtWidget::NativeConstruct()
{
	Super::NativeConstruct();

}

void ULenghtWidget::UpdateLenght(int32 Value)
{
	LenghtLabel->SetText(FText::AsNumber(Value));
}
